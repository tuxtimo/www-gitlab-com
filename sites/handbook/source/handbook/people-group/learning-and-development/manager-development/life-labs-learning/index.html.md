---
layout: handbook-page-toc
title: LifeLabs Learning Training for Managers at GitLab
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What is LifeLabs Learning

GitLab L&D is excited to collaborate with LifeLabs Learning to deliver their [Manager Core 1](https://lifelabslearning.com/companies/workshops/) training to GitLab managers. The 5-part training covers `coaching`, `feedback skills`, `productivity & prioritization`, `effective 1:1's` and conclused with a `manager intensive` 3 months after the core traingings are delivered. 

Read more about [LifeLabs Learning](https://lifelabslearning.com/) on their website.

## Get involved

### FY23 Manager Core 1 

13 managers completed the [Manager Core 1](https://drive.google.com/file/d/1MJmxjrMSSCq3lWOOks-vMnzdzPucI0jp/view) pilot program in February/March of FY23. Key reflections and resources can be found in [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/365)

From May to September, a second cohort of 40 manangers will complete the program. Team members can view the most up to date program information in [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/60).

To express interest in a future training from LifeLabs, please add yourself to the waitlist using [this issue](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/53)

### What to expect

As participants in the training, managers can expect the following:

1. Attend 5 training sessions for a total of 2 hours each
1. Review post-session work and apply skills with their direct reports
1. Participant in async discussions and reminders in the [#lifelabs-learning Slack channel](https://app.slack.com/client/T02592416/C036Y740SKY/thread/C5P8T9VQX-1587584276.009700)
1. Subscribe to the [lifelabs label](https://gitlab.com/gitlab-com/people-group/learning-development/training-curriculum/-/issues/?label_name%5B%5D=lifelabs) to review post-session work via GitLab issues


## Questions?

Team members with questions should reach out in the [#lifelabs-learning Slack channel](https://app.slack.com/client/T02592416/C036Y740SKY/thread/C5P8T9VQX-1587584276.009700). Non-team members with questions should reach out directly to [LifeLabs Learning](https://lifelabslearning.com/).





